# Build dependency docker image to use as a base in .gitlab-ci.yml
# Otherwise the latter runs out of RAM and times out

FROM haskell:8.8.2

LABEL maintainer="greenrd@greenrd.org"

RUN mkdir /prebuild /builds
RUN ln -s /prebuild /builds/greenrd

WORKDIR /builds/greenrd/my-blog

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
COPY testing.list /etc/apt/sources.list.d
RUN apt-get update && apt-get -t testing install -y openssh-client xz-utils tidy linkchecker setpriv emacs-nox
RUN useradd -g users -M -N builder
COPY stack.yaml Setup.hs my-blog.cabal LICENSE ./
RUN chown -R builder:users ..
ENV HOME=/builds/greenrd

USER builder
RUN mkdir ~/.ssh
RUN ssh-keyscan -t rsa github.com >> ~/.ssh/known_hosts
RUN ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
RUN git clone https://gitlab.com/ambrevar/emacs-webfeeder.git
RUN stack config set system-ghc --global true
RUN stack setup
RUN stack install --only-dependencies
USER root