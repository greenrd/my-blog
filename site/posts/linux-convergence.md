---
post-title:  "Linux convergence? I’m hoping for it!"
post-author: "Robin Green"
post-date: 2014-08-31
image: "tux_in_android_robot_costume.png"
---

**UPDATE:** This [has now happened!](http://arstechnica.com/gadgets/2014/09/hack-runs-android-apps-on-windows-mac-and-linux-computers/)

---

Bought myself a nice 5.1 speaker system a few months ago – so I don’t use our tinny crappy old computer speakers any more, yay! But the irony is, I usually just plug my Nexus 7 into it (using the bog standard noisey stereo input, instead of the higher quality digital input cable) to play music from [Spotify](https://www.spotify.com/)!

That’s because, while Spotify is available for Linux, it’s only _officially_ supported at the moment on Android, and it’s been a bit of a pain to install on [Fedora Linux](http://fedoraproject.org/) / [QubesOS](https://qubes-os.org/) – there are two community-created install methods, one of which is currently known to be broken, and neither of which worked for me the last time I tried them.

I only use Skype on my Nexus too, for a similar reason. Skype on Linux is behind – it doesn’t support group calls. Microsoft, who acquired Skype Technologies in 2011, have said they are working on it. Microsoft? Working on commercial software for desktop Linux? That’s a new one on me!

But the irony is, both Fedora and Android are Linux!

Well, they’re Linux underneath – the operating system infrastructure on top of the Linux kernel, and how developers typically write applications, is quite different in each. There are now two main branches of Linux in the wild: desktop/server Linux, which is further subdivided into numerous distributions, and Android. “Server Linux” is conceptually just Desktop Linux with bits removed (and/or left unused) – it’s not really a separate thing, except for marketing purposes. There’s also a couple more branches of Linux which have just started to appear in the mobile market: [Firefox OS](https://en.wikipedia.org/wiki/Firefox_OS) and [Sailfish OS](https://en.wikipedia.org/wiki/Sailfish_OS); and two more nascent ones which haven’t been commercially released on real devices yet: [Ubuntu Touch](https://en.wikipedia.org/wiki/Ubuntu_Touch) and [Tizen](http://en.wikipedia.org/wiki/Tizen).

In terms of [open source](https://en.wikipedia.org/wiki/Open-source_software), my sense is that desktop Linux and Firefox OS are the most open, and Tizen (if it ever gets properly released), followed by Sailfish OS, will be the least open, with Android somewhere in the middle in practice, although I may be wrong on that. I’m talking about the _platforms_ here, not third-party apps – the open source application ecosystem seems to me to be far stronger on desktop and server Linux than on Android.

One of these is not like the others, though! Ubuntu Touch is an attempt to bring Ubuntu to mobile platforms – a bit like Microsoft’s ideas of platform convergence that it tried to force onto its users (not entirely successfully!) with Windows 8 – so it’s not a clean break like the other mobile operating systems.^[Sailfish also runs Android apps, but since they’re both mobile platforms at the moment, that’s not as interesting or impressive to me as the idea of Ubuntu Touch.]

Also, Firefox OS is pushing the concept of HTML5 apps – although these can also be downloaded like regular mobile apps, so it’s not completely clear to me where Firefox OS really stands in the convergence spectrum.^[Sailfish also pushes HTML5, although not to the extent that Firefox OS does.]

Finally – and perhaps most importantly – as I posted about yesterday on Google+, Google has said that [Chromebooks will get the ability to run Android apps](http://www.engadget.com/2014/06/25/google-to-bring-android-apps-to-the-chromebook/)^[Presumably, this means without using an Android emulator – which is slow, as far as I know.] by the end of this year – and Chromium OS, on which Chrome OS is closely based, is open source. So _hopefully_ that ability will appear in Chromium OS, and then be ported to other Linux distributions – solving my Skype and Spotify dilemma.

![A Samsung Chromebook. Photo by Alex Washburn/Wired [CC-BY-1.0](http://creativecommons.org/licenses/by/1.0), via Wikimedia Commons](../images/1024px-Chromebook.jpg "A Samsung Chromebook")

The music industry probably won’t be bothered, as they’ve already agreed that Spotify users on tablets can have the same rights to stream music as Spotify on desktops – unlike Spotify users on mobile phones.^[But how do [phablets](https://en.wikipedia.org/wiki/Phablet) fit into Spotify’s streaming licensing scheme? We simply must be told!]

But goodness knows what Microsoft will make of that! Wailing and gnashing of teeth, probably!