#!/usr/bin/emacs --script
(load "~/.emacs")
(require 'webfeeder)
(let ((dir "./public/posts"))
  (webfeeder-build
   "atom.xml"
   dir
   "http://greenrd.org/posts/"
   (directory-files dir nil ".*\.html" t)
   :title "Pragmatic Magpie"
   :description "Robin Green's blog")
  )
