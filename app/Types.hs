{-# LANGUAGE CPP                   #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeFamilies          #-}

module Types where

import           Data.Aeson                 as A
import           Data.Aeson.TH
import           Data.Maybe
import qualified Data.Text                  as T
import           Data.Text.Lazy             (pack, toStrict)
import           Data.Time
import           Development.Shake.Classes
import           GHC.Generics               (Generic)
import           Prelude hiding (span)
import           Slick.Extra
import           Text.Blaze.Html.Renderer.Text
import           Text.Blaze.Html5
import           Text.Blaze.Html5.Attributes (class_)

renderToDay :: FormatTime t => t -> String
renderToDay = formatTime defaultTimeLocale "%B %e, %Y"

renderToHtml :: UTCTime -> T.Text
renderToHtml dt = toStrict . (pack ("<!-- " ++ formatTime defaultTimeLocale "%F" dt ++ " -->") <>) . renderHtml . (span ! class_ "date") $ datePieces
  where
    datePieces = do
      span ! class_ "day" $ toHtml $ formatTime defaultTimeLocale "%e" dt ++ " "
      span ! class_ "month" $ do
        toHtml $ formatTime defaultTimeLocale "%b" dt
        span . toHtml . drop 3 $ formatTime defaultTimeLocale "%B" dt
      span ! class_ "year" $ toHtml $ ' ' : formatTime defaultTimeLocale "%Y" dt

newtype IndexWrap =
  IndexWrap
    { posts         :: [Post]
    } deriving (Generic, Show)

instance FromJSON IndexWrap
instance ToJSON IndexWrap

data Post =
  Post
    { title   :: String
    , author  :: T.Text
    , date    :: UTCTime
    , url     :: String
    , image   :: Maybe T.Text
    , content :: String
    } deriving (Generic, Eq, Ord, Show)

instance FromJSON Post where
  parseJSON =
    withObject "metadata" $ \o -> do
      title   <- o .:  "post-title"
      author  <- o .:  "post-author"
      date'   <- o .:  "post-date"
      url     <- o .:  "url"
      image   <- o .:? "image"
      content <- o .:  "content"

      let dateM = parseTimeM True defaultTimeLocale "%Y-%m-%d" date'
      let date = fromMaybe (parseTimeOrError True defaultTimeLocale "%B %e, %Y" date') dateM

      return $
        Post title author date url image content

instance ToJSON Post where
  toJSON (Post tl au dt ul im cn) =
    A.object $
      [ "post-title"  A..= tl
      , "post-author" A..= au
      , "post-date"   A..= renderToDay dt
      , "date-html"   A..= renderToHtml dt
      , "url"         A..= ul
      , "content"     A..= cn
      ] ++ maybe [] ((:[]) . ("image" A..=)) im

newtype BlogWrap =
  BlogWrap
    { blog :: [Post]
    } deriving (Generic, Show)

instance FromJSON BlogWrap
instance ToJSON BlogWrap

data About =
  About
    { abrwTitle       :: String
    , abrwDescription :: String
    , abrwUrl         :: String
    , abrwContent     :: String
    } deriving (Generic, Show)

deriveJSON defaultOptions { fieldLabelModifier = dropPrefix 4 } ''About

-- | Represents About page
-- with additional data
data AboutWrap =
  AboutWrap
    { abTitle       :: String
    , abDescription :: String
    , abUrl         :: String
    , abContent     :: String
    } deriving (Generic, Show)

deriveJSON defaultOptions { fieldLabelModifier = dropPrefix 2 } ''AboutWrap

newtype AboutFilePath =
  AboutFilePath String
  deriving (Show, Eq, Generic, Hashable, Binary, NFData)

newtype PostFilePath =
  PostFilePath String
  deriving (Show, Eq, Generic, Hashable, Binary, NFData)
