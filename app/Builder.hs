{-# LANGUAGE CPP                       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE Rank2Types                #-}
{-# LANGUAGE ScopedTypeVariables       #-}

module Builder( buildIndex
              , buildOther
              , buildAbout
              , buildPost
              , myMarkdownOptions
              , html5Options
              ) where

import           Data.Aeson                   as A
import           Data.List                    (sortBy)
import qualified Data.Text                    as T
import           Development.Shake
import           Development.Shake.FilePath
import           Slick
import           Slick.Extra
import           Slick.Pandoc
import           Text.Pandoc.Extensions
import           Text.Pandoc.Highlighting
import           Text.Pandoc.Options
import           Text.Pandoc.Readers.Markdown (readMarkdown)
import           Text.Pandoc.Writers.HTML     (writeHtml5String)
import           Types

buildIndex :: (EntityFilePath PostFilePath -> Action Post)
           -> FilePath
           -> Action ()
buildIndex postCache out = do
  posts     <- traverse (postCache . EntityFilePath) =<< fileNames "site" "posts" "//*.md"
  indexTpl  <- compileTemplate' "site/templates/index.html"

  let postsSorted = sortBy (flip (\ p1 p2 -> compare (date p1) (date p2))) posts

  let indexWrap = IndexWrap { posts = postsSorted }
      indexHTML = T.unpack $ substitute indexTpl (toJSON indexWrap)
  writeFile' out indexHTML

buildAbout :: (EntityFilePath AboutFilePath -> Action About)
           -> FilePath
           -> Action ()
buildAbout  aboutCache out = do
  about'<- traverse (aboutCache . EntityFilePath) =<< fileNamesByPattern "site/about.md"
  aboutTpl  <- compileTemplate' "site/templates/about.html"

  let about''   = head about'
      aboutWrap = AboutWrap { abContent     = abrwContent about''
                            , abUrl         = abrwUrl about''
                            , abTitle       = abrwTitle about''
                            , abDescription = abrwDescription about''
                            }
      aboutHTML = T.unpack $ substitute aboutTpl (toJSON aboutWrap)

  writeFile' out aboutHTML

buildPost :: (EntityFilePath PostFilePath -> Action Post)
          -> FilePath
          -> Action ()
buildPost postCache out = do
  let srcPath = destToSrc "site" out -<.> "md"
      postURL = srcToURL srcPath
  post <- postCache (EntityFilePath srcPath)
  template <- compileTemplate' "site/templates/post.html"
  writeFile' out . T.unpack $ substitute template (toJSON post)

buildOther filename = do
    liftIO $ putStrLn $ unwords ["source", show source, "template", show template]
    need [source, template]
    content  <- readFile' source >>=  markdownToHTML myMarkdownOptions html5Options . T.pack
    template <- compileTemplate' template
    writeFile' filename . T.unpack $ substitute template $ toJSON content
  where
    source   = destToSrc "site" filename -<.> "md"
    template = "site/templates/generic.html"

myMarkdownToHTML :: T.Text -> Action Value
myMarkdownToHTML  =
  loadUsing (readMarkdown myMarkdownOptions) (writeHtml5String html5Options)

myMarkdownOptions = def { readerExtensions = exts }
 where
  exts = mconcat
    [ extensionsFromList
      [ Ext_yaml_metadata_block
      , Ext_fenced_code_attributes
      , Ext_inline_code_attributes
      , Ext_auto_identifiers
      , Ext_inline_notes
      , Ext_multiline_tables
      , Ext_implicit_figures
      ]
    , githubMarkdownExtensions
    ]

html5Options :: WriterOptions
html5Options =
  def { writerHighlightStyle = Just tango
      , writerExtensions     = writerExtensions def
      }
