{-# LANGUAGE CPP                   #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Main where

import           Builder
import           Control.Monad
import           Development.Shake
import           Development.Shake.FilePath
import           Development.Shake.Util        (shakeArgsPruneWith)
import           Slick
import           Slick.Extra
import           Slick.Serve
import           System.Console.GetOpt
import           System.Directory
import           System.Environment

data Flags = Preview deriving (Eq)

flags :: forall a. [OptDescr (Either a Flags)]
flags = [ Option "" ["preview"] (NoArg $ Right Preview) "running as preview"]

buildRules :: Foldable t => t Flags -> Rules ()
buildRules flags = do
  let isPreviewMode = Preview `elem` flags
      entitiesDir = "site"
      atomFeed = "public/posts/atom.xml"
      elisp = "gen-feed.el"

  when isPreviewMode $
    action $ runAfter $ liftIO $ void $ do
      putStrLn "Running with Preview"
      serverStart "public" "127.0.0.1" 3030 serverHandler

  do
      aboutCache <- jsonCache' $ loadEntity myMarkdownOptions html5Options entitiesDir
      postsCache <- jsonCache' $ loadEntity myMarkdownOptions html5Options entitiesDir

      want ["lint"]

      phony "site" $ do
          let reqDirectories =
               [ "static"
               , "posts"
               , "public/robots.txt"
               , "public/index.html"
               , "public/about.html"
               , "public/404.html"
               ]

          need reqDirectories

          pages <- fmap (("public"</>) . (-<.>"html")) <$> getDirectoryFiles "site" ["*.md"]
          liftIO $ print pages
          need $ atomFeed : pages

      "linkchecker.out" ~> do
          need ["site"]
          command_ [Cwd "public"] "linkchecker" ["--no-status", "."]

      atomFeed ~> do
        posts <- fmap (("public/posts"</>) . (-<.>"html")) <$> getDirectoryFiles "site/posts" ["*.md"]
        need $ elisp : posts
        cmd_ ["." </> elisp]

      phony "tidy" $ do
          need ["site"]
          inputs <- getDirectoryFiles "public" ["//*.html"]
          forM_ inputs $ \input ->
            command_ [Cwd "public"] "tidy" ["-config", "../tidy.init", input]

      phony "lint" $
          need ["tidy", "linkchecker.out"]

      phony "static" $ do
          staticFiles <-
            getDirectoryFiles "." [ "site/css//*"
                                  , "site/js//*"
                                  , "site/images//*"
                                  , "site/webfonts//*"
                                  , "site/robots.txt" ]
          need (("public" </>) . dropDirectory1 <$> staticFiles)

      phony "clean" $ removeFilesAfter "public" ["//*"]

      [ "public/css//*",
        "public/js//*",
        "public/images//*",
        "public/webfonts//*",
        "public/robots.txt"
        ] |%> \out ->
          copyFileChanged (entitiesDir </> dropDirectory1 out) out

      "public/about.html" %> buildAbout aboutCache
      "posts" ~> requireEntity entitiesDir "posts" "//*.md"
      "public/posts//*.html" %> buildPost postsCache
      "public/*.html" %> buildOther
      "public/index.html" %> buildIndex postsCache

  want ["site"]

runShakeBuilder :: ShakeOptions
                -> [OptDescr (Either String Flags)]
                -> IO ()
runShakeBuilder shOpts flags =
 shakeArgsPruneWith
    shOpts (pruner "public") flags $
      \flags targets -> do
        let rls = Just $ buildRules flags
        return rls

main :: IO ()
main = do
  shakeArgs <- getArgs
  cwd       <- getCurrentDirectory

  let shOpts = shakeOptions { shakeVerbosity = Normal
                            , shakeCommandOptions = [EchoStdout True]
                            , shakeStaunch = True
                            }

  runShakeBuilder
    shOpts
    flags
